# Jednoduchý Profil

Statická stránka s profilem účastnice kurzu. Staticky generovaná pomocí Hugo s automatickým deploymentem do Gitlab pages. 

# Použití

Nainstalujeme statický generátor Hugo. Viz https://gohugo.io/getting-started/installing/. 

Použité téma je vloženo jako Git submodul, proto je potřeba jej nejprve nainicializovat. 

`git submodule update --init --recursive`

## Lokální vývoj

Spustíme lokální vývojový server: 

`hugo server`

## Generování statické stránky

Statickou verzi vygenerujeme příkazem: 

`hugo`

Výsledek najdeme ve složce `public`.
